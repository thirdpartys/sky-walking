package org.apache.skywalking.apm.agent;

import org.apache.skywalking.apm.agent.core.boot.AgentPackageNotFoundException;
import org.apache.skywalking.apm.agent.core.plugin.PluginBootstrap;

public class SkywalkingAgentTest {

    public static void main(String[] args) throws AgentPackageNotFoundException {
        new PluginBootstrap().loadPlugins();
    }

}
